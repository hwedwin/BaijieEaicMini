$.app({
    data:{
        entity:null
    },
    on:{
        action:{
            "fileChange":function (file) {
                var me = this;

                $.apiForm("site.Sys.uploadFile",{file:file}).then(function (value) {
                    var entity = me.data.entity;
                    entity.value.url = value;

                    me.setData({
                        action:{
                            'entityPropChange' : me.data.entity
                        }
                    })
                })
            }
        }
    }

});