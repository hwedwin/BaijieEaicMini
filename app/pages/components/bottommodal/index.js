// pages/components/bottommodal/index.js
//本组件实现从底部弹出的遮罩对话框
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        isShow : {
            type:Boolean,
            value:false
        },
        height:{
            type:Number,
            value:430
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        showStatus:false
    },
    options: {
        addGlobalClass: true,
    },
    observers:{
        "isShow" : function (val) {
            var me = this;
            if( val ) me.showModal();
            else me.hideModal();
        }
    },

    /**
     * 组件的方法列表
     */
    methods: {
        showModal: function () {

            var me = this;
            if (me.data.showStatus) return;
            // 显示遮罩层
            var animation = wx.createAnimation({
                duration: 200,
                timingFunction: "linear",
                delay: 0
            });
            //this.animation = animation;
            animation.translateY(300).step();
            me.setData({
                animationData: animation.export(),
                showStatus: true
            });
            setTimeout(function () {
                animation.translateY(0).step();
                me.setData({
                    animationData: animation.export()
                })
            }.bind(me), 200)
        },

        hideModal: function () {

            var me = this;
            if (!me.data.showStatus) return;

            // 隐藏遮罩层
            var animation = wx.createAnimation({
                duration: 200,
                timingFunction: "linear",
                delay: 0
            });
            //this.animation = animation
            animation.translateY(300).step();
            me.setData({
                animationData: animation.export(),
            });
            setTimeout(function () {
                animation.translateY(0).step();
                me.setData({
                    animationData: animation.export(),
                    showStatus: false
                })
            }.bind(me), 200)

        },
        onclickHide: function (e) {

            var me = this;

            me.hideModal();

        }
    }
});
