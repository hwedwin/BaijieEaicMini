var $ = getApp().globalData.eui;
$.component({
	/**
	 * 组件的属性列表
	 */
	properties: {

	},


	/**
	 * 组件的初始数据
	 */
	data: {
		$isPage:true,
		userMode: 0,
        loginUser: null,

		"images": [{
			"title":"待付款",
			"icon":"/images/unpay.png",
			tabindex:1,
			count:0
		},{
			"title":"待收货",
			"icon":"/images/unreceive.png",
			tabindex:2,
			count:0
		},/*{
            "title":"拼团中",
            "icon":$.getConfig("image_root") + "/icon/inpin.png"//拼团中
        },*/{
			"title":"已完成",
			"icon":"/images/finished.png",
			tabindex:3,
			count:0
		},{
			"title":"全部订单",
			"icon":"/images/all.png",
			tabindex:0,
			count:0
		}],
	},



	/**
	 * 组件的方法列表
	 */
	methods: {
		onViewRecvAddr: function (e) {

            var me = this;

			getApp().getLoginUser(function (oUser) {
				if (!(oUser && oUser.id)) {
					$.alertBox("提示", "请先登录").then(function () {
						var fn = $.getConfig("onLogin");
						fn();
					})

				} else {
					$.go("/pages/person/recvaddress/index");
				}
			})

    
        },
        onLogin: function (e) {
            $.go("/pages/person/login");
        },
        onViewCart: function (e) {
    
            var me = this;

			getApp().getLoginUser(function (oUser) {
				if (!(oUser && oUser.id)) {
					$.alertBox("提示", "请先登录").then(function () {
						var fn = $.getConfig("onLogin");
						fn();
					})

				} else {
					$.go("/pages/index?tabindex=2");
				}
			})

    
        },
        onClickDistCenter: function (e) {
            var me = this;
            getApp().getLoginUser(function (oUser) {
				if (!(oUser && oUser.id)) {
					$.alertBox("提示", "请先登录").then(function () {
						var fn = $.getConfig("onLogin");
						fn();
					})

				} else {
					if( oUser.isdist ){
						$.go("/pages/person/distcenter/index");
					}else{
						$.go("/pages/person/distcenter/apply/index");
					}

				}
			})

            
        },
        onConsole:function(e){
            var me = this;
            $.go("/pages/person/console");
        },
        onOrder: function (e) {
            var me = this;
            var index = $.toInt(e.currentTarget.dataset.index);

			getApp().getLoginUser(function (oUser) {
				if (!(oUser && oUser.id)) {
					$.alertBox("提示", "请先登录").then(function () {
						var fn = $.getConfig("onLogin");
						fn();
					})

				} else {
					$.go("/pages/order/index?tabIndex="+index);
				}
			})
            

        },

		notifyLogin:function(oUser){
			var me = this;
			$.log("==========+++ 收到Login事件："+$.toJson(oUser));
			me.setData({"loginUser":oUser});
			me.afterLogin();
		},

		afterLogin:function () {
			var me = this;

			var loginUser = me.data.loginUser;
			if( loginUser && loginUser.id){
				$.api("mall.Order.GetCounts",{})
					.then(function (value) {
						var images = me.data.images;
						images[0].count = $.toInt(value.unpay);
						images[1].count = $.toInt(value.unreceive);
						images[2].count = $.toInt(value.finished);
						images[3].count = $.toInt(value.total);
						me.setData({
							"images":images
						})
					})
			}

		}
	},
	lifetimes: {
		// 生命周期函数，可以为函数，或一个在methods段中定义的方法名
		attached: function () {
			var me = this;

			getApp().getLoginUser(function (oUser) {
				me.setData({"loginUser":oUser});
				if( oUser && oUser.id ){
					me.afterLogin();
				}
			})
		},
		detached: function () {
			var me = this;

		},
	},
});