// pages/components/navigateBar/index.js
var $ = getApp().globalData.eui;
Component({
	/**
	 * 组件的属性列表
	 */
	properties: {
		title:{
			type:String,
			value:""
		},
		location:{
			type:Boolean,
			value:false
		},
		camara:{
			type:Boolean,
			value:false
		}
	},

	/**
	 * 组件的初始数据
	 */
	data: {
		canBack:false
	},
	options: {
		addGlobalClass: true,
	},

	/**
	 * 组件的方法列表
	 */
	methods: {
		onBack:function (e) {
			$.back();
        },
        onHome: function (e) {
            wx.reLaunch({
                url: '/pages/index',
                })
           
        }
	},
	lifetimes: {
		// 生命周期函数，可以为函数，或一个在methods段中定义的方法名
		attached: function () {
			var me = this;
            //console.log("pages:::"+$.toJson(getCurrentPages()));
            var title = me.data.title;
            if (!title) title = $.getConfig("appName");
			me.setData({
				
                "canBack": (getCurrentPages().length > 1 ? true : false),
                "title" : title
			})
		},
		detached: function () {
			var me = this;

		},
	},
});
