@echo off

set work_path=%~dp0
set filecount=0

D: 

cd %work_path%

echo "%work_path%js.txt"
del "%work_path%js.txt"

set DestExt=*.dev.js

for /f "delims=" %%i   in ('dir  /b/a-d/s  %work_path%\%DestExt%')  do (
echo %%i >> "%work_path%js.txt"
)

rem 开始压缩
echo starting.... %work_path%js.txt

for /f %%i in (%work_path%js.txt) do (
	call:compress %%i %work_path%
)

echo finished! 共处理 %filecount% 个文件

d:
cd %work_path%

pause 

goto:eof


:compress
rem echo compress %1
set fn=%1
set ext=%fn:~-6%
rem echo target %fn:~0,-6%min.js
if %ext%==dev.js call:work %fn% %fn:~0,-6%min.js
goto:eof

:work
set f=%~nx1
set t=%~nx2
set dn=%~dp1
cd %dn:~0,-1%


echo processing %1
set /a filecount=filecount+1
java -jar D:\工具\开发工具\js\yuicompressor-2.4.8.jar --type js --charset utf-8 %f% -o %t%


goto:eof






