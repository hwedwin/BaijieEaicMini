var $ = getApp().globalData.eui;
Component({
	/**
	 * 组件的属性列表
	 */
	properties: {
		loginContext:{
			type:Object,
			value:{}
		}
	},

	/**
	 * 组件的初始数据
	 */
	data: {
		thisPage:[],
	},

	/**
	 * 组件的方法列表
	 */
	methods: {},
	lifetimes: {
		// 生命周期函数，可以为函数，或一个在methods段中定义的方法名
		attached: function () {
			var me = this;
			$.api("xwsh.Page.GetContent",{"url":"/memberpurchase"})
				.then(function (value) {
					var arr=[];
					if( value ) arr = $.fromJson(value);
					if( !$.isArray(arr)) arr=[];
					me.setData({thisPage:arr});

				});

		},
		detached: function () {
			var me = this;

		},
	},
});
