var $ = getApp().globalData.eui;
Component({
	/**
	 * 组件的属性列表
	 */
	properties: {
		value:{
			type:Number,
			value:0
		}
	},

	/**
	 * 组件的初始数据
	 */
	data: {},
	options: {
		addGlobalClass: true,
	},
	/**
	 * 组件的方法列表
	 */
	methods: {},
	lifetimes: {
		// 生命周期函数，可以为函数，或一个在methods段中定义的方法名
		attached: function () {
			var me = this;

		},
		detached: function () {
			var me = this;

		},
	},
})
