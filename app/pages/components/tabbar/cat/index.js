var $ = getApp().globalData.eui;
Component({
	/**
	 * 组件的属性列表
	 */
	properties: {
		activeCat:{
			type:Number,
			value:0
		}
	},

	/**
	 * 组件的初始数据
	 */
	data: {
		"catindex": 0,
		"cats": [],
		"products": [],
		"cond": {
			"pageindex": 1,
			"pagesize": 8,
			"catid": 0,
			isnew:0,
			ishot:0
		},
		"totalpage": 0,
		search_icon:$.getConfig("image_root")+"/search-16.png"
	},
	options: {
		addGlobalClass: true,
	},
	observers:{
		"activeCat"	:function (aid) {
			var me = this;
			if( aid === 0 ){
				me.setData({"catindex":0});
			}else if( aid===-1){
				me.setData({"catindex":1});
			}else if( aid===-2){
				me.setData({"catindex":2});
			}
		}
	},
	/**
	 * 组件的方法列表
	 */
	methods: {
		onClickCat: function (e) {

			var me = this;

			var index = $.toInt(e.currentTarget.dataset.index);
			me.setData({
				"catindex": index
			});

            me.refresh();

		},
		onViewProduct: function (e) {

			var me = this;

			$.go("/pages/product/detail?id=" + e.currentTarget.dataset.id);

		},
		onScrollViewUpload: function (e) {

			var me = this;

			me.loadNextPage();

		},
		onScrollViewDownRefresh: function (e) {

			var me = this;

			me.refresh();

		},
		refresh: function () {

			var me = this;
			me.data.cond.pageindex = 0;
			me.loadNextPage();
		},
		loadNextPage: function () {

			var me = this;
			var cond = me.data.cond;
			if (me.data.cats.length) {
				var catindex=me.data.catindex;
				if( catindex===1){
					cond.catid=0;
					cond.isnew=1;
					cond.ishot=0;
				}else if( catindex===2){
					cond.catid=0;
					cond.ishot=1;
					cond.isnew=0;
				}else if( catindex===0){
					cond.catid=0;
					cond.ishot=0;
					cond.isnew=0;
				}else{
					cond.catid = me.data.cats[catindex-3].id;
					cond.ishot=0;
					cond.isnew=0;
				}

				cond.pageindex++;
				if (cond.pageindex === 1) me.data.products = []; //第一页要重置

				$.api("mall.Product.ListPage", cond)
					.then(function (value) {
						var p = me.data.products;
						console.log("after merchant.ListPage:" + $.toJson(value));
						$.each(value.datas, function (product) {
							var cover = $.toString(product.cover);

							if (cover && !(cover.indexOf("http:") == 0 || cover.indexOf("https:") == 0)) {
								product.cover = $.getConfig("server_url")  + cover;
							}
						});
						if ($.count(value.datas) > 0)
							p = p.concat(value.datas);
						me.setData({
							"products": p,
							"totalpage": value.totalpage
						});

					}, function (reason) {
						console.log(reason.message);
					})


			}

		},
		onSearch:function (e) {
			$.go("/pages/product/search/index")
		}
	},
	lifetimes: {
		attached: function () {
			var me = this;
			var entity = me.data.entity;

			$.api("mall.Cat.LoadAll", {}).then(function (value) {
				me.setData({
					"cats": value
				});
				for(var i=0;i<value.length;i++){
					if( $.toInt(value[i].id) === me.data.activeCat ){
						me.setData({"catindex":i+3});
					}
				}
				if (value.length) {
					me.refresh();
				}
			});
		},
		detached: function () {
			// 在组件实例被从页面节点树移除时执行
		},
	},
});
