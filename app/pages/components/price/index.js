var $ = getApp().globalData.eui;
var Decimal = require("../../../decimal.js");
//本组件实现价格的显示
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        //价格
        value : {
            type:Number,
            value:0
        },
        //货币类型
        currency:{
            type:String,
            value:"￥"
        },
        //是否为原价（带删除线，灰色）
        isOrigin:{
            type:Boolean,
            value:false
        },
        //单位
        unit:{
            type:String,
            value:""//元
        },
        //是否显示“起”
        useqi:{
            type:Boolean,
            value:false
        },
        isWhite:{
            type:Boolean,
            value:false
        },
        //显示大小
        size:{
            type:String,
            value:"" //取值：空字符串、small、large、huge
        },
        //显示风格
        display:{
            type:String,
            value:""//normal或空：普通 emphasize:强调
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        integral:0,//整数部分
        decimal:0//小数部分
    },
    options: {
        addGlobalClass: true,
    },
    observers:{
        "value":function (vPrice) {
            var me = this;
            vPrice = new Decimal(vPrice).toNumber();
            $.log("=====> vPrice:" + vPrice);
            var i = $.toInt(vPrice);
            var d = $.toInt((vPrice - i) * 100);
            $.log("======> i=" + i + " d=" + d);
            //var x = $.toInt(vPrice*1000) % 10;
            //if( x >= 5 ) d ++;//四舍五入
            me.setData({
                integral:i,
                decimal:""+(d<10?"0":"")+d
            });

        }
    },

    /**
     * 组件的方法列表
     */
    methods: {}
})
