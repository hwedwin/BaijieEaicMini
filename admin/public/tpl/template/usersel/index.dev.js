eui.app({
    data:{
        meta:{
            datacount:0,
            pagecount:0,
            datas:[],
            allsel:false,
            cond:{
                pageindex:1,
                pagesize:10,
                search_text:""
            }
        },
        empty:{
            title:"没有可供选择的用户",
            image:"/tpl/theme/default/images/nodata.png"
        }
    },
    on:{
        "meta.cond.pageindex" : function () {
            var me = this;
            me.loadPage();
        },
        "meta.cond.search_text" : function () {
            var me = this;
            me.loadPage();
        }
    },
    onLoad:function () {
        var me = this;

        me.loadPage();
    },
    loadPage:function () {
        var me = this;

        eui.api("oa.User.ListPage",me.data.meta.cond).then(function (data) {
            me.setData({
                meta:{
                    datas:data.datas,
                    pagecount:data.totalpage,
                    datacount:data.totalcount
                }
            });
        })
    },

    onSel:function (e) {
        var me = this;
        var id = eui.toInt( eui.dom(e.src).data("id") );

        var selcount=0;
        eui.each(me.data.meta.datas,function (item) {
            if( eui.toInt(item.id) === id ){
                item.checked = !item.checked;
            }
            if( item.checked ) selcount++;
        });

        me.data.meta.allsel = selcount === me.data.meta.datas.length;
        me.setData();
    },
    onSelAll:function (e) {
        var me = this;

        me.data.meta.allsel = !me.data.meta.allsel;
        eui.each(me.data.meta.datas,function (item) {
            item.checked = me.data.meta.allsel;
        });


        me.setData();
    },
    onSelUser:function (e) {
        var me = this;
        var index = $.toInt(e.data.index);
        var user = me.data.meta.datas[index];
        me.setData({
            action:{
                selectUser:user
            }
        });
        e.done();
    }
});

