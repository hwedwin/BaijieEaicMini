//此组件实现图片的自适应缩放
var $ = getApp().globalData.eui;
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        url:{
            type:String,
            value:""
        },
        widthFirst:{
            type:Boolean,
            value:true
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        width:0,
        height:0
    },
    options: {
        addGlobalClass: true,
    },

    /**
     * 组件的方法列表
     */
    methods: {
        onImageLoad:function (e) {
            var me = this;

            var px2rpx = 1;//fix me later

            $.log("==e.width="+e.detail.width+" e.height="+e.detail.height);

            var originWidth = e.detail.width * px2rpx,
                originHeight=e.detail.height *px2rpx,
                ratio = originWidth/originHeight ;
            var viewWidth = 470,viewHeight = 165, viewRatio = viewWidth/viewHeight;
            $.log("====== viewRatio="+viewRatio);
            if(ratio>=viewRatio){
                if(originWidth>=viewWidth){
                    viewHeight = viewWidth/ratio;
                }else{
                    viewWidth = originWidth;
                    viewHeight = originHeight
                }
            }else{
                if(originWidth>=viewWidth){
                    viewWidth = viewRatio*originHeight
                }else{
                    viewWidth = viewRatio*originWidth;
                    viewHeight = viewRatio*originHeight;
                }
            }

            $.log("==width="+viewWidth+" height="+viewHeight);
            me.setData({
                width:viewWidth,
                height:viewHeight
            })
        }
    }
});
