$.app({
    data:{
        widgets:[]
    },
    onWidgetDragStart:function (e) {
        var me = this;
        var widget = me.data.widgets[$.toInt(e.data.index)];
        e.dataTransfer.setData("Widget",$.toJson(widget));
    }
});