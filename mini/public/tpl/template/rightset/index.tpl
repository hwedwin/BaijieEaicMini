<div>
<div u-each="rightset" u-item="item" u-index="index" >
    <div class="right-cat" >
        <div class="right-item " u-click="onCheck" data-item="${item.path}">
            <i class="fa fa-square-o"></i><span class="lp-m">${item.name}</span>
        </div>
    </div>

    <div u-if="item.children_leaf">
        <div class="right-cat">
            <div u-each="item.children" u-item="child" u-click="onCheck" class="lp-n right-item right-leaf" data-item="${child.path}}">
                <i class="fa fa${ child.checked ? '-check',''}-square-o"></i><span class="lp-m">${child.name}</span>
            </div>
        </div>
    </div>
    <div u-else="">

        <div u-each="item.children" u-item="child1">
            <div class="lp-n right-cat">
                <div class="right-item " u-click="onCheck" data-item="${child1.path}">
                    <i class="fa fa-square-o"></i><span class="lp-m">${child1.name}</span>
                </div>
            </div>

            <div u-if="child1.children_leaf" class="right-cat">
                <div u-each="child1.children" u-item="child2"
                     u-click="onCheck"
                     class="right-item right-leaf" style="padding-left:30px;"
                     data-item="${child2.path}">
                    <i class="fa fa${child2.checked? '-check':''}-square-o"></i><span
                            class="lp-m">${child2.name}</span>
                </div>

            </div>

            <div u-else="">
                <div u-each="child1.children" u-item="child3">
                    <div class="lp-n right-cat" style="padding-left:30px;" data-item="${child3.path}">
                        <i class="fa fa-square-o"></i><span class="lp-m">${child3.name}</span>
                    </div>
                    <div u-if="child3.children_leaf" class="right-cat">
                        <div u-each="child3.children" u-item="child4"
                             u-click="onCheck"
                             class="lp-n right-item right-leaf"
                             style="padding-left:60px;" data-item="${child4.path}">
                            <i class="fa fa${child4.checked ? '-check',''}-square-o"></i><span
                                    class="lp-m">${child4.name}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>