<div>
<div class="container-fluid titlebar" >
    <div class="row">
        <div class="col-md-6">
            <span u-v="winbar.title" class="align-middle titlebar-title"></span>
        </div>
        <div class="col-md-6 text-right ">

            <a u-click="onBack" class="hide titlebar-close">
                <i class="fa fa-remove align-middle"></i>
                <span class="align-middle">关闭</span>
            </a>

        </div>
    </div>
</div>
</div>