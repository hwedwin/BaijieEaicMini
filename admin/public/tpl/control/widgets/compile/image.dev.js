(function($){
    return {
        compile:function (uicPage,entity,htmlParent) {
            var me = this;

            var div = new $.UIC_HtmlElement("div");
            htmlParent.add(div);
            div.setProp("class","image");

            var img = new $.UIC_HtmlElement("img");
            div.add(img);

            //=== 样式
            var g = new $.StyleGen(entity);
            var style = g.gen();

            var cls = $.toString(me._genClass(uicPage,entity));
            if( style.cls ) cls += " " + style.cls;

            if( cls ) img.setProp("class",cls);
            if( style.style ) img.setProp("style",style.style);

            if( entity.value.url ) img.setProp("src",entity.value.url);

            var mode="";
            switch($.toInt(entity.value.mode))
            {
                case 0:
                    mode="scaleToFill";
                    break;
                case 1:
                    mode="aspectFit";
                    break;
                case 2:
                    mode="aspectFill";
                    break;
                case 3:
                    mode="widthFix";
                    break;
                default:
                    mode="scaleToFill";
                    break;
            }
            img.setProp("mode",mode);
            img.setProp("lazy-load","true");

        },
        _genClass:function (uicPage,entity) {
            return "cover-image";
        }
    }
})(eui)