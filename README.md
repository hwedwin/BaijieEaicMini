# BaijieEaicMini

#### 介绍
百捷伊可小程序商城系统，是一款基于百捷伊可平台开发的开源电商系统。

#### 软件架构
软件包括3套程序：

1. 微信小程序前端，源码位于app目录
2. 微信小程序后台，源码位于mini目录
3. 商城管理后台，源码位于admin目录

#### 功能介绍

- 商品管理，支持商品分类、商品多规格、商品上架下架、到期自动下架
- 订单管理，订单支付、发货、取消、删除等
- 会员管理，会员列表、会员关系查看、关系解绑等
- 运营管理，广告轮播、公司新闻
- 微信管理，小程序设置、微信支付设置
- 拼团，按数量拼团、按人数拼团
- 分销，支持无限级分销，二级返佣
- 海报，支持在后台自定义海报模板，前台选择模板自动生成海报

#### 产品截图

![首页](https://www.baijienet.com/tpl/theme/default/images/product/wxmini/home.png){:width="350px" height="500px"}

![商城](https://www.baijienet.com/tpl/theme/default/images/product/wxmini/mall.png){:width="350px" height="500px"}

![产品](https://www.baijienet.com/tpl/theme/default/images/product/wxmini/product.png){:width="350px" height="500px"}

![个人中心](https://www.baijienet.com/tpl/theme/default/images/product/wxmini/personcenter.png){:width="350px" height="500px"}

![收货地址](https://www.baijienet.com/tpl/theme/default/images/product/wxmini/recvaddress.png){:width="350px" height="500px"}

![订单](https://www.baijienet.com/tpl/theme/default/images/product/wxmini/order.png){:width="350px" height="500px"}

![分销中心](https://www.baijienet.com/tpl/theme/default/images/product/wxmini/distcenter.png){:width="350px" height="500px"}

![提现](https://www.baijienet.com/tpl/theme/default/images/product/wxmini/withdraw.png){:width="350px" height="500px"}

![购物车](https://www.baijienet.com/tpl/theme/default/images/product/wxmini/cart.png){:width="350px" height="500px"}

#### 技术支持

更多关于产品安装及使用的问题，请加QQ群(群号：276228406)或关注公众号(百捷伊可)获得技术支持。