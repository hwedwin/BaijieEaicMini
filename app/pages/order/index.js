var $ = getApp().globalData.eui;
$.page({
    data: {
        "tabs": [
            {"title":"全部","cond": {"pageindex": 1, "pagesize": 10,statuscat:0}, totalcount:0,totalpage:1,"datas": []},
            {"title":"待付款","cond": {"pageindex": 1, "pagesize": 10,statuscat:1}, totalcount:0,totalpage:1,"datas": []},
            {"title":"待收货","cond": {"pageindex": 1, "pagesize": 10,statuscat:2}, totalcount:0,totalpage:1,"datas": []},
            {"title":"已完成","cond": {"pageindex": 1, "pagesize": 10,statuscat:3}, totalcount:0,totalpage:1,"datas": []},
        ],
        "deleteIcon":$.getConfig("image_root")+"/trash.png",
        "activeTab": 0
    },
    onLoad: function (options) {

        var me = this;
        var activeTab = $.toInt(options.tabIndex);
        me.setData({ activeTab: activeTab});
        me.refresh(activeTab);
    },

    onClickStatus: function (e) {

        var me = this;

        var index = $.toInt(e.currentTarget.dataset.index);
        if (index != me.data.activeTab) {
            me.setData({activeTab: index});
            me.refresh(index);
        }

    },
    onSwipChange: function (e) {

        var me = this;

        me.setData({
            activeTab: e.detail.current
        });
        me.refresh(me.data.activeTab);

    },
    onRemove:function(e){
        var me = this;
        var id = $.toInt(e.currentTarget.dataset.id);
        var key=e.currentTarget.dataset.key;
        $.confirmBox("询问","确定要删除此订单？",function () {
            $.api("mall.Order.Remove",{id:id})
                .then(function (value) {
                    me.refresh(key);
                })
        })
    },
    onPay:function(e){
        var me = this;
        var id = $.toInt(e.currentTarget.dataset.id);
        var key=e.currentTarget.dataset.key;
        $.api("mall.Order.Pay", {
            orderid: id
        })
            .then(function (value) {
                wx.requestPayment({
                    'timeStamp': value.timeStamp,
                    'nonceStr': value.nonceStr,
                    'package': value.package,
                    'signType': 'MD5',
                    'paySign': value.paySign,
                    'success': function (res) {
                        $.alertBox("提示", "支付成功")
                            .then(function () {
                                me.refresh(key);
                                me.refresh("unrec");//刷新已支付的界面
                            })
                    },
                    'fail': function (res) {
                        console.log("支付 res:" + $.toJson(res));
                        //支付 res:{"err_code":"-1","err_desc":"调用支付JSAPI缺少参数: package","errMsg":"requestPayment:fail"}
                        //$.errorBox("错误", "支付失败：" + res.errMsg + " 原因：" + res.err_desc);
                        wx.showToast({
                            title: '支付失败',
                            icon: 'error',
                            duration: 2000,
                            success:function () {

                            }
                        })
                    },
                    'complete': function (res) {
                        //$.back();
                    }
                })
            });
    },
    onRecv:function(e){
        var me = this;
        var id = $.toInt(e.currentTarget.dataset.id);
        var key=e.currentTarget.dataset.key;
        $.confirmBox("询问","确定收到货物了吗？")
            .then(function () {
                $.api("mall.Order.Confirm",{"id":id})
                    .then(function (value) {
                        me.refresh(me.data.activeTab);
                    })
            })
    },
    onViewDeliver:function(e){
        var me = this;
        var id = $.toInt(e.currentTarget.dataset.id);
        $.go("/pages/order/deliver?orderid="+id);
    },
    onViewFlow:function(e){
        var me = this;
        var id = $.toInt(e.currentTarget.dataset.id);
        $.go("/pages/order/flow?orderid="+id);
    },
    onScrollViewUpload: function (e) {
        var me = this;
        $.log("--- e="+$.toJson(e));
        me.loadNextPage($.toInt(e.currentTarget.dataset.index));
    },
    onScrollViewDownRefresh: function (e) {
        var me = this;
        me.refresh($.toInt(e.currentTarget.dataset.index));
    },

    refresh: function (cat) {

        var me = this;
        me.data.tabs[cat].cond.pageindex = 0;
        me.loadNextPage(cat);
    },

    loadNextPage: function (cat) {

        var me = this;
        var cond = me.data.tabs[cat].cond;

        $.log("--- old pageindex="+cond.pageindex);
        var datas = me.data.tabs[cat].datas;
        var pageindex=cond.pageindex;

        pageindex++;
        if (pageindex === 1) datas = []; //第一页要重置
        if( pageindex <= me.data.tabs[cat].totalpage ){
            $.log("--- pageindex="+pageindex+" totalpage:"+me.data.tabs[cat].totalpage+" cat:"+cat);
            $.api("mall.Order.ListPage", cond)
                .then(function (value) {
                    //$.log("加载到数据 "+cat+":"+$.toJson(value.datas));
                    $.each(value.datas,function (v) {
                        v.status = $.toInt(v.status);
                        v.paystatus = $.toInt(v.paystatus);
                        v.deliverstatus = $.toInt(v.deliverstatus);
                        v.confirmstatus = $.toInt(v.confirmstatus);
                    });

                    $.each(value.datas, function (data) {
                        $.each(data.products, function (product) {
                            var icon = product.icon;
                            if (icon.indexOf("/") === 0) {
                                product.icon = $.getConfig("server_url") + icon;
                            }
                        });
                    });
                    datas = datas.concat(value.datas);
                    //var obj={};
                    //obj["tabs["+cat+"].datas"] = datas;
                    //obj["tabs["+cat+"].totalpage"] = value.totalpage;
                    var tabs = me.data.tabs;
                    tabs[cat].datas = datas;
                    tabs[cat].totalpage=value.totalpage;
                    tabs[cat].totalcount=value.totalcount;
                    tabs[cat].cond.pageindex = pageindex;
                    me.setData({"tabs":tabs});

                }, function (reason) {
                    console.log(reason.message);
                })
        }

    }

});