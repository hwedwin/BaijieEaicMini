<div>
    <div class="danxuan-page">
        <div class="field">
            <label>标题</label>
            <div class="control">
                <input type="text" class="input" u-v="entity.value.title" u-change="onChange"/>
            </div>

        </div>
        <div class="field">
            <label>选项</label>
            <div class="control">
                <div class="vm-m choice" u-each="entity.value.choices" u-index="index">
                    <div class="remove">
                        <i class="fa fa-remove" data-index="${index}" u-click="onRemoveChoice" title="删除"></i>
                    </div>
                    <div class="item">
                        <input type="text" class="input" u-v="entity.value.choices[${index}]" u-change="onChoiceChange"/>
                    </div>
                </div>
            </div>

            <div class="vm-n">
                <a type="button" class="button " u-click="onAddChoice">
                    <i class="fa fa-plus rm-m"></i>添加
                </a>
            </div>
        </div>
    </div>

</div>