var $ = getApp().globalData.eui;

$.page({

    /**
     * 页面的初始数据
     */
    data: {
        "productid" : 0,
        "pins": [],
        "cond": {
            "pageindex": 1,
            "pagesize": 8,
            "productid": 0
        },
        "totalpage": 0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var me = this;
        wx.setNavigationBarTitle({
            title:"进行中的拼团"
        });
        var productid = $.toInt(options.productid);
        me.setData({
            "productid" : productid,
            "cond.productid" : productid
        });
        me.refresh();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    onJoinPin:function(e){
        var me = this;
        var id = $.toInt(e.currentTarget.dataset.id);
        var skuid = $.toInt(e.currentTarget.dataset.skuid);
        if (!me.data.logined) {
            $.errorBox("错误", "请先登录", function () {
                $.go("/pages/person/login");
            });
        }
        else
            $.go("/pages/product/pin/joinpin?pinid="+id+"&skuid="+skuid);
    },
    onScrollViewUpload: function (e) {

        var me = this;

        me.loadNextPage();

    },
    onScrollViewDownRefresh: function (e) {

        var me = this;

        me.refresh();

    },
    refresh: function () {

        var me = this;
        me.data.cond.pageindex = 0;
        me.loadNextPage();
    },

    loadNextPage: function () {

        var me = this;
        var cond = me.data.cond;
        cond.pageindex++;
        if (cond.pageindex === 1) me.data.products = []; //第一页要重置

        $.api("mall.Pin.LoadInPin", cond)
            .then(function (value) {
                var p = me.data.pins;
                //console.log("after product.ListPage:"+$.toJson(value));
                $.each(value.datas,function (pin) {
                    var icon = pin.icon;
                    //$.log("== icon:"+icon);
                    if( !(icon.indexOf("http:") == 0 || icon.indexOf("https:") == 0) ){
                        pin.icon = $.getConfig("server_url")+"/"+$.getConfig("tenantid")+icon;
                    }
                });
                if ($.count(value.datas) > 0)
                    p = p.concat(value.datas);
                me.setData({
                    "pins": p,
                    "totalpage": value.totalpage
                });
            }, function (reason) {
                console.log(reason.message);
            })
    }
})