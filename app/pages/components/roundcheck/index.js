var $ = getApp().globalData.eui;

//本组件实现圆形checkbox
//入参：
//  value: 是否选中
//  title: 显示标题
//事件：
//  change:function(value){}

Component({
    /**
     * 组件的属性列表
     */
    properties: {
        title:{
            type:String,
            value:""
        },
        value:{
            type:Boolean,
            value:false
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        checked:false,
        img_check:$.getConfig("image_root")+"/checked.png",
        img_uncheck:$.getConfig("image_root")+"/uncheck.png"
    },
    options: {
        addGlobalClass: true,
    },
    observers:{
        "value":function (v) {
            var me = this;
            me.setData({checked:v});
        }
    },

    /**
     * 组件的方法列表
     */
    methods: {
        onChange:function (e) {
            var me = this;
            var checked = $.toInt(e.currentTarget.dataset.checked);
            me.setData({checked:checked});
            me.triggerEvent("change", {value:checked}, {});
        }
    }
})
