<div>
    <div class="dialog-cover" u-click="onCancel" u-keep="dialog.type !=''">
        <div
                u-ani-in=""
                u-ani-out=""
                u-name="alert"
                u-share="{'dialog':'dialog'}"
                u-keep="dialog.type=='alert'"
                u-view="/template/dialog/alert/index"
                u-ecss="/template/dialog/alert/index"
                u-app="/template/dialog/alert/index">

        </div>
        <div
                u-ani-in=""
                u-ani-out=""
                u-name="confirm"
                u-share="{'dialog':'dialog'}"
                u-keep="dialog.type=='confirm'"
                u-view="/template/dialog/confirm/index"
                u-ecss="/template/dialog/confirm/index"
                u-app="/template/dialog/confirm/index">

        </div>
        <div
                u-ani-in=""
                u-ani-out=""
                u-name="warning"
                u-share="{'dialog':'dialog'}"
                u-keep="dialog.type=='warning'"
                u-view="/template/dialog/warning/index"
                u-ecss="/template/dialog/warning/index"
                u-app="/template/dialog/warning/index">

        </div>
        <div
                u-ani-in=""
                u-ani-out=""
                u-name="error"
                u-share="{'dialog':'dialog'}"
                u-keep="dialog.type=='error'"
                u-view="/template/dialog/error/index"
                u-ecss="/template/dialog/error/index"
                u-app="/template/dialog/error/index">

        </div>
    </div>
</div>