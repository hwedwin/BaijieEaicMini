$.app({
    data:{
        entity:null,
        numbers:[1,2,3,4,5],
        overindex:-1
    },
    on:{
        action:{
            'entityPropChange':function (entity) {
                var me=this;
                if( entity.id === me.data.entity.id )
                    me.setData({
                        entity: entity
                    });
            }
        }
    },
    onMouseOver:function (e) {
        var me = this;
        var index = $.toInt(e.data.index);
        me.setData({
            overindex:index
        })
	},
	onMouseLeave:function (e) {
        var me = this;
        me.setData({
            overindex:-1
        })
	}
});