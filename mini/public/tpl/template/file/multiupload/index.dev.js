$.app({
    data:{
        maxcount:4,//最多允许上传数
        images:[]
    },
    onLoad:function () {
        var me = this;
    },
    onAdd:function (e) {
        var me = this;
        //
    },
    onFileChange: function (e) {
        var me = this;

        var file = e.src.files[0];

        if( file && file.type.indexOf("image") === 0){
            $.log("选取了一个图片...");
            me.addImage(file);
        }


        /*var fnFindImage = function () {
            $.finde = e;
            var parent = e.src.parentNode.parentNode.parentNode;
            var nodecount = parent.childNodes.length;
            for (var i = 0; i < nodecount; i++) {
                var node = parent.childNodes[i];
                if (node.nodeType === 1 && node.tagName.toLowerCase() === 'img') {
                    return node;
                    //break;
                }
            }
            return null;
        };
        var image = fnFindImage();


        var img = new Image();
        img.onload = function () {
            // 图片原始尺寸
            var originWidth = this.width;
            var originHeight = this.height;
            // 最大尺寸限制
            var maxWidth = 1420;
            var maxHeight = 1100;
            // 目标尺寸
            var targetWidth = originWidth;
            var targetHeight = originHeight;
            // 图片尺寸超过400x400的限制
            if (originWidth > maxWidth || originHeight > maxHeight) {
                if (originWidth / originHeight > maxWidth / maxHeight) {
                    // 更宽，按照宽度限定尺寸
                    targetWidth = maxWidth;
                    targetHeight = Math.round(targetWidth * (originHeight / originWidth));
                } else {
                    targetHeight = maxHeight;
                    targetWidth = Math.round(targetHeight * (originWidth / originHeight));
                }
            }
            image.width = targetWidth;
            image.height = targetHeight
        };

        var reader = null;
        if( window.FileReader ){
            reader = new FileReader();
            reader.onload = function (e) {
                //$.teste = e;
                img.src = e.target.result;
                image.src = e.target.result
            };
        }



        // 选择的文件是图片
        if (file.type.indexOf("image") === 0) {
            me.setData({ file: file,hint:"点击替换图片" });
            if( reader ) reader.readAsDataURL(file);
        }*/
    },
    addImage:function (file) {
        var me = this;

        var obj={};
        obj["file"] = file;
        obj["src"]="";
        var images = me.data.images;
        images.push(obj);


        var reader = new FileReader();
        reader.onload = function () {
            obj["src"] = this.result;
            me.setData({
                images:images
            });
        };

        reader.readAsDataURL(file);
    },
    onRemove:function (e) {
        var me = this;
        var index = $.toInt(e.data.index);
        var images = me.data.images;
        images.splice(index,1);
        me.setData({
            images:images
        });
    }
});