<div>
    <div class="form-date" u-click="onPop" u-outside-click="onHide">
        <div class="datemenu">
            <ul>
                <li>
                    <a u-v="selday_str" ></a>
                    <ul class="poplay" style="left:${pop_pos};top:${pop_top}">
                        <li class="level tool">
                            <div class="level-left">
                                <span class="datebtn" u-click="onPrevYear">
                                    <i class="fa fa-angle-double-left "></i>
                                </span>
                                <span class="datebtn" u-click="onPrevMonth">
                                    <i class="fa fa-angle-left "></i>
                                </span>
                            </div>
                            <div class="level-right">
                                <span class="datebtn" u-click="onNextMonth">
                                    <i class="fa fa-angle-right  "></i>
                                </span>
                                <span class="datebtn" u-click="onNextYear">
                                    <i class="fa fa-angle-double-right "></i>
                                </span>
                            </div>
                            <div class="curdate text-center">
                                ${showday_text}
                            </div>
                        </li>
                        <li class="date-row date-title" >
                            <span u-each="titles" u-item="title" >
                                ${title}
                            </span>
                        </li>
                        <li class="date-row" u-each="rows" u-item="row" u-index="rowindex">
                            <span u-each="row" u-item="day" u-index="dayindex"
                                  data-row="${rowindex}" data-col="${dayindex}"
                                  u-click="onSel"
                                  class="${day.isPrev} ${day.isNext} ${day.isSel} ${day.isCell} ${day.isToday}">
                                ${day.date.getDate()}
                            </span>
                        </li>

                    </ul>
                </li>

            </ul>
        </div>

    </div>
</div>