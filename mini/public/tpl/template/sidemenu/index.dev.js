eui.app({
	data:{
		menu:null,
		curpath:""
	},
	onLoad:function(){
		var me = this;

	},
	onMenu:function(e){
		var me = this;
		var dom = eui.dom(e.src);
		var path = dom.data("path");

		if( path ){

			var item = null;
            eui.each(me.data.menu,function(menu){


                if( menu.path === path ){
                    menu.expand = (menu.expand ? "" : "expand");
                    item = menu;
                    return true;
                }
                eui.each(menu.funcs,function(child){
                	if( child.path === path ){
                		item = child;
                		return true;
					}
				})
            });


            if( item ){

            	var data = {};
                if( !item.funcs )
                	data['curpath'] = path;

                me.setData(data);

                if( eui.count(item.funcs) === 0 ){
                    eui.go(path,{});
				}

			}

		}

	}

});